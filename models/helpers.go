package models

import (
	"github.com/charmbracelet/bubbles/key"
	"github.com/charmbracelet/lipgloss"
)

/* ---------- lipgloss styles ---------- */
var titleStyle = lipgloss.NewStyle().
	//Foreground(lipgloss.Color("#FFFDF5")).Padding(0, 1)
	Foreground(lipgloss.Color("#43b54c")).Height(7).PaddingBottom(0)

var borderStyle = lipgloss.NewStyle().
	BorderStyle(lipgloss.NormalBorder()).
	BorderForeground(lipgloss.Color("#26803e"))

var appStyle = lipgloss.NewStyle()

var helpStyle = lipgloss.NewStyle().Foreground(lipgloss.Color("#129100")).Padding(1, 2)

var statusMessageStyle = lipgloss.NewStyle().
	Foreground(lipgloss.AdaptiveColor{Light: "#04B575", Dark: "#04B575"}).Render

/* ---------- end lipgloss styles ---------- */

type listKeyMap struct {
	insertItem key.Binding
}

func NewListKeyMap() *listKeyMap {
	return &listKeyMap{
		insertItem: key.NewBinding(
			key.WithKeys("a"), key.WithHelp("a", "insert item"),
		),
	}
}
