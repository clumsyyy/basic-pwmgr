package models

import (
	"strings"

	"teapwmgr/database"
)

type textComponent struct {
	currentAccount database.Account
}

func (tc *textComponent) Update(a database.Account) {
	tc.currentAccount = a
}

func (tc textComponent) View() string {
	var sb strings.Builder
	sb.WriteString("Name:        " + tc.currentAccount.Name + "\n\n")
	sb.WriteString("Username:    " + tc.currentAccount.Username + "\n\n")
	sb.WriteString("Password:    " + tc.currentAccount.Password + "\n\n")
	sb.WriteString("Description: " + tc.currentAccount.Desc)
	return sb.String()
}
