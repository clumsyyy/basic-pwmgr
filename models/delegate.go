package models

import (
	"github.com/charmbracelet/bubbles/key"
	"github.com/charmbracelet/bubbles/list"
	"github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"

	"teapwmgr/database"
)

type delegateKeyMap struct {
	choose key.Binding
	remove key.Binding
}

func (d delegateKeyMap) ShortHelp() []key.Binding {
	return []key.Binding{d.choose, d.remove}
}
func (d delegateKeyMap) FullHelp() [][]key.Binding {
	return [][]key.Binding{{d.choose, d.remove}}
}

func newDelegateKeyMap() *delegateKeyMap {
	return &delegateKeyMap{
		choose: key.NewBinding(key.WithKeys("enter"), key.WithHelp("enter", "choose")),
		remove: key.NewBinding(key.WithKeys("x"), key.WithHelp("x", "delete")),
	}
}

func newItemDelegate(keys *delegateKeyMap) list.DefaultDelegate {
	d := list.NewDefaultDelegate()

	lightGreenColor := lipgloss.Color("#329e16")
	darkGreenColor := lipgloss.Color("#1f610e")

	d.Styles.SelectedTitle.Foreground(lightGreenColor)
	d.Styles.SelectedTitle.BorderForeground(lightGreenColor)
	d.Styles.SelectedDesc.BorderForeground(lightGreenColor)

	d.Styles.SelectedDesc.Foreground(darkGreenColor)

	d.UpdateFunc = func(msg tea.Msg, m *list.Model) tea.Cmd {
		var title string
		account, ok := m.SelectedItem().(database.Account)
		if !ok {
			title = ""
		} else {
			title = account.Name
		}

		switch msg := msg.(type) {
		case tea.KeyMsg:
			switch {
			case key.Matches(msg, keys.choose):
				return m.NewStatusMessage(statusMessageStyle("You chose " + title))

			case key.Matches(msg, keys.remove):
				index := m.Index()
				m.RemoveItem(index)
				if len(m.Items()) == 0 {
					keys.remove.SetEnabled(false)
				}

				return m.NewStatusMessage(statusMessageStyle("Deleted " + title))
			}
		}
		return nil
	}

	help := []key.Binding{keys.choose, keys.remove}

	d.ShortHelpFunc = func() []key.Binding {
		return help
	}

	d.FullHelpFunc = func() [][]key.Binding {
		return [][]key.Binding{help}
	}

	return d
}
