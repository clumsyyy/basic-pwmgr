package models

import (
	"log"
	"strings"

	"github.com/charmbracelet/bubbles/key"
	"github.com/charmbracelet/bubbles/list"
	"github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"

	"teapwmgr/database"
)

// teaPwmgrModel
// teaPwmgrModel is the main bubbletea model of this program. It
// contains all the other models and a state value to control
// what is displayed
type teaPwmgrModel struct {
	initialized    bool
	title          string
	accountList    list.Model
	accountDisplay textComponent
	focused        int
	keys           *listKeyMap
	delegateKeys   *delegateKeyMap
}

// teaPwmgrModel.Init()
// This is used to setup the bubbletea application
func (m teaPwmgrModel) Init() tea.Cmd {
	return tea.EnterAltScreen
}

// teaPwmgrModel.Update()
// Called on every change to the application, the overall state
// will get updated here on key presses and the model view
// returned
func (m teaPwmgrModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	log.Printf("model.Update() called")
	//m.accountList.Styles.Title.GetHeight()
	var cmds []tea.Cmd

	switch msg := msg.(type) {

	case tea.WindowSizeMsg:
		log.Printf(
			"model.Update() has WindowSizeMsg, width = %d, height = %d",
			msg.Width, msg.Height,
		)
		//w, h := appStyle.GetFrameSize()

		//log.Printf("h = %d, w = %d\n", h, w)
		if !m.initialized {
			m.initModel(msg.Width-50, msg.Height-18)
			return m, nil
		}
		// m.accountList.SetHeight(msg.Height - 50) // useless line, don't think it does anything?

	case tea.KeyMsg:
		log.Printf("model.Update() has KeyMsg")
		if m.accountList.FilterState() == list.Filtering {
			break
		}

		switch {
		case key.Matches(msg, m.keys.insertItem):

			/*
				case key.Matches(msg, m.keys.insertItem):
					m.delegateKeys.remove.SetEnabled(true)
			*/
		}
	}
	log.Printf("model.Update() made it past msg switch")
	if !m.initialized {
		return m, nil
	}

	newListModel, cmd := m.accountList.Update(msg)
	m.accountList = newListModel
	m.accountDisplay.currentAccount = m.accountList.SelectedItem().(database.Account)
	cmds = append(cmds, cmd)
	return m, tea.Batch(cmds...)
}

func (m teaPwmgrModel) View() string {
	if !m.initialized {
		return "loading..."
	}

	var doc strings.Builder

	titleElement := lipgloss.JoinHorizontal(
		lipgloss.Top,
		titleStyle.Render(m.title),
	)

	doc.WriteString(titleElement + "\n\n")
	tempStyle := lipgloss.NewStyle().Width(48)
	listWidthStyle := lipgloss.NewStyle().Width(48)

	contentBlock := lipgloss.JoinHorizontal(
		lipgloss.Top,
		listWidthStyle.Render(m.accountList.View()),
		tempStyle.Render(m.accountDisplay.View()),
	)

	doc.WriteString(contentBlock + "\n\n")

	return borderStyle.Render(appStyle.Render(doc.String()))
}

func (m *teaPwmgrModel) initModel(width int, height int) {
	/* create the list */
	accounts := database.GetFakeAccounts()

	delegate := newItemDelegate(m.delegateKeys)
	mainList := list.New(accounts, delegate, width, height)
	mainList.SetShowTitle(false)

	currentAccount := accounts[0].(database.Account)

	accountDisplay := textComponent{currentAccount: currentAccount}

	mainList.AdditionalFullHelpKeys = func() []key.Binding {
		return []key.Binding{
			m.keys.insertItem,
		}
	}

	m.accountList = mainList
	m.accountDisplay = accountDisplay
	m.initialized = true
}

func NewTeaPwmgrModel() teaPwmgrModel {
	listKeys := NewListKeyMap()
	delegateKeys := newDelegateKeyMap()

	return teaPwmgrModel{
		initialized: false,
		title:       GetTitle(),
		//accountList:    mainList,
		//accountDisplay: accountDisplay,
		keys:         listKeys,
		delegateKeys: delegateKeys,
	}
}
