package models

import (
	"strings"
)

func GetTitle() string {
	var sb strings.Builder
	sb.WriteString("   _____       _____                                \n")
	sb.WriteString("  / ____|     |  __ \\                               \n")
	sb.WriteString(" | |  __  ___ | |__) |_      ___ __ ___   __ _ _ __ \n")
	sb.WriteString(" | | |_ |/ _ \\|  ___/\\ \\ /\\ / / '_ ` _ \\ / _` | '__|\n")
	sb.WriteString(" | |__| | (_) | |     \\ V  V /| | | | | | (_| | |   \n")
	sb.WriteString("  \\_____|\\___/|_|      \\_/\\_/ |_| |_| |_|\\__, |_|   \n")
	sb.WriteString("                                          __/ |     \n")
	sb.WriteString("                                         |___/ \n")
	return sb.String()
}
