package main

import (
	"fmt"
	"log"
	"os"

	"github.com/charmbracelet/bubbletea"

	"teapwmgr/models"
)

func main() {
	fp := setupLoggingFile()
	log.SetOutput(fp)
	defer fp.Close()

	p := tea.NewProgram(models.NewTeaPwmgrModel())
	_, err := p.Run()
	if err != nil {
		fmt.Printf("Error: %s\n", err.Error())
	}

	dataDir := os.Getenv("XDG_DATA_HOME")
	if dataDir == "" {
		fmt.Printf("need to use fallback XDG_DATA_HOME\n")
	}
}

func setupLoggingFile() *os.File {
	fp, err := os.OpenFile("./logfile", os.O_APPEND|os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		log.Fatalf("err opening log, err = %s\n", err.Error())
	}
	return fp
}
