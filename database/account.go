package database

import (
	"github.com/charmbracelet/bubbles/list"
)

type Account struct {
	Id       int
	Name     string
	Username string
	Password string
	Desc     string
}

/* bubbles.ListItem functions */
func (a Account) FilterValue() string {
	return a.Name
}

func (a Account) Title() string {
	return a.Name
}
func (a Account) Description() string {
	if len(a.Desc) > 32 {
		return a.Desc[:31]
	}
	return a.Desc
}

func GetFakeAccounts() []list.Item {
	return []list.Item{
		Account{
			Name:     "gmail",
			Username: "jimbob123@gmail.com",
			Password: "as;lkdn234lkn234",
			Desc:     "my gmail account",
		}, Account{
			Name:     "gmail2",
			Username: "jimbobsaccount2@gmail.com",
			Password: "lk345lkn.s,ikjdsflkjsdf",
			Desc:     "hide this one, it's a super secret account that shouldn't be viewable by normal people and this is going to be a super long description",
		}, Account{
			Name:     "netflix",
			Username: "jimbob123@gmail.com",
			Password: "netflixpassword1",
			Desc:     "for videos",
		}, Account{
			Name:     "hulu",
			Username: "jimbob123@gmail.com",
			Password: "HULUpassword1",
			Desc:     "for videos",
		}, Account{
			Name:     "Amazon",
			Username: "jimbob123@gmail.com",
			Password: "amazonpassword123",
			Desc:     "shopping and stuff",
		}, Account{
			Name:     "gmail3",
			Username: "jimbob123@gmail.com",
			Password: "as;lkdn234lkn234",
			Desc:     "my gmail account",
		}, Account{
			Name:     "gmail4",
			Username: "jimbob123@gmail.com",
			Password: "as;lkdn234lkn234",
			Desc:     "my gmail account",
		}, Account{
			Name:     "gmail5",
			Username: "jimbob123@gmail.com",
			Password: "as;lkdn234lkn234",
			Desc:     "my gmail account",
		}, Account{
			Name:     "gmail6",
			Username: "jimbob123@gmail.com",
			Password: "as;lkdn234lkn234",
			Desc:     "my gmail account",
		}, Account{
			Name:     "gmail7",
			Username: "jimbob123@gmail.com",
			Password: "as;lkdn234lkn234",
			Desc:     "my gmail account",
		}, Account{
			Name:     "gmail8",
			Username: "jimbob123@gmail.com",
			Password: "as;lkdn234lkn234",
			Desc:     "my gmail account",
		}, Account{
			Name:     "gmail9",
			Username: "jimbob123@gmail.com",
			Password: "as;lkdn234lkn234",
			Desc:     "my gmail account",
		}, Account{
			Name:     "gmail10",
			Username: "jimbob123@gmail.com",
			Password: "as;lkdn234lkn234",
			Desc:     "my gmail account",
		}, Account{
			Name:     "gmail11",
			Username: "jimbob123@gmail.com",
			Password: "as;lkdn234lkn234",
			Desc:     "my gmail account",
		}, Account{
			Name:     "gmail12",
			Username: "jimbob123@gmail.com",
			Password: "as;lkdn234lkn234",
			Desc:     "my gmail account",
		}, Account{
			Name:     "gmail13",
			Username: "jimbob123@gmail.com",
			Password: "as;lkdn234lkn234",
			Desc:     "my gmail account",
		}, Account{
			Name:     "gmail14",
			Username: "jimbob123@gmail.com",
			Password: "as;lkdn234lkn234",
			Desc:     "my gmail account",
		}, Account{
			Name:     "gmail15",
			Username: "jimbob123@gmail.com",
			Password: "as;lkdn234lkn234",
			Desc:     "my gmail account",
		},
	}
}
